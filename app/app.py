from flask import Flask
from flask import render_template
import psycopg2
import redis
import datetime
import os

app = Flask(__name__)

conn = psycopg2.connect(
    dbname=os.environ.get("POSTGRES_DB"),
    user=os.environ.get("POSTGRES_USER"),
    password=os.environ.get("POSTGRES_PASSWORD"),
    host=os.environ.get("POSTGRES_HOST")
)
cur = conn.cursor()

r = redis.Redis(host=os.environ.get("REDIS_HOST"), port=6379, db=0)

@app.route('/')
def index():
    try:
        r.incr('counter')

        visit_time = datetime.datetime.now()
        cur.execute("INSERT INTO visits (visit_time) VALUES (%s)", (visit_time,))
        conn.commit()

        visit_count = r.get('counter').decode('utf-8')

        return render_template('index.html', visit_count=visit_count, visit_time=visit_time)
    except Exception as e:
        conn.rollback() 
        return f"An error occurred: {str(e)}"
if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')
