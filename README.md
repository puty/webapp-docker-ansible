# DevOPS Project

This project aims to create a simple web application using two data sources (PostgreSQL and Redis). The application will then be packaged into a Docker container and deployed using an Ansible playbook.

## Project Structure

1. **Counter Application**
    - A simple HTTP application that stores the current date and time in a PostgreSQL database and also maintains a page view count in Redis.
    - After each page view, the page view count in Redis increases by 1.
    - The resulting data is displayed as an HTML page.

2. **Ansible Playbook**
    - A playbook for deploying data sources (PostgreSQL and Redis) for the Counter application and deploying the Counter application itself with its configuration.
    - Communication between the application and data sources must occur over an internal Docker network.
    - The playbook is executable on Ubuntu 22.04.

## Process

### 1. Counter Application
   - In the GIT repository for the Counter application, you will find the source code and configuration of the application.
   - The `.gitlab-ci.yml` file is used to create a Docker image and store it in the GitLab Container Registry.

### How to start project

```bash
cd app
docker-compose up --build
```
### Example web page
![alt text](image.png)
